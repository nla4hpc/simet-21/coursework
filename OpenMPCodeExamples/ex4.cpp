#include <omp.h>
#include <stdio.h>
#include <chrono>
#include <iostream>


int main (){
    int n=40000;
    double values[n];
    
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    #pragma omp parallel for schedule(dynamic)
    for (int j=0; j<n; j++) {
	    double tmp = 0.0;
	    for( int i=0; i<j; i++) {
	    	tmp+= double(j) * double(j);
	    }
        values[j] = tmp;
    }
    #pragma omp barrier
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;

}
