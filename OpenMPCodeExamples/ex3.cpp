#include <omp.h>
#include <stdio.h>


int main(){

  int x=0, y=0;
  
  #pragma omp parallel 
  {
    
  #pragma omp single 
  {
    
    #pragma omp task depend(inout: x)
    {
      x=5;printf("hello world x:%d\n", x);
    }
    
    #pragma omp task depend(inout: x)
    {
      x++;printf("hello again y:%d\n", y);
    }
    
    #pragma omp task depend(in: x)
    {
      x++;printf("hello again:%d\n", x);
    }
    
    
  }
  }
  return 0;
}
