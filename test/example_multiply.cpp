#include "gtest/gtest.h"
#include "my_math.h"

/** @brief Addition tester

    This unit test checks the functionality of the add_numbers routine.
    The original code is from: https://github.com/pothitos/gtest-demo-gitlab.git
    
    @author H. Anzt, KIT
    @date March 2019
    */
TEST(example, multiply)
{
    double res;
    res = multiply_numbers(1.0, 2.0);
    ASSERT_NEAR(res, 2.0, 1.0e-11);
}
